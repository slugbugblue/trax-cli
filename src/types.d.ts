/** Type information.
 * @copyright 2022-2023
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

/** Color is a single character to represent white or black. */
type Color = 'w' | 'b'

/** Colorize is a fancy function for colorizing text. */
type Colorize = (text: string, def?: string | number) => string

/** Colorer is a little too fancy. Hence the gnarly typescript. */
type Colorer = {
  (text: string, def?: string | number): string
  black: Colorize
  command: Colorize
  default: Colorize
  error: Colorize
  fatal: Colorize
  help: Colorize
  id: Colorize
  variable: Colorize
  optional: Colorize
  short: Colorize
  white: Colorize
}

/** A note with a move number */
type GameNote = {
  move: number
  note: string
}

/** Notes saved in the game object in the CLI. */
type GameNotes = GameNote[]

/** Treat the save state as an opaque object,
 * produced by save() and fed into restore().
 */
type SaveState = {
  id: string
  move: number
  turn: number
  over: boolean
  left: number
  right: number
  top: number
  bottom: number
  notation: string
  tiles: string
  path: string
  invalid: boolean
}

/** Representations of the different ways a tile can curve.
 * Matches the symbols used in Trax notation.
 */
type Slash = '/' | '\\' | '+'

/** Threat definition. */
type Threat = {
  depth: number
  pattern: string
  rx: RegExp
  value: number
}

/** A single tile on the board. */
type Tile = {
  id: TileId
  loc: Point
  type: TileType
  move: number
  seq: number
}

/** When a tile is dropped, this object represents the results. */
type TileDrop = {
  dropped: Tile[]
  notation: string
  valid: boolean
}

/** A TileId is just a string. */
type TileId = string

/** Invalid tiles are represented by 'x'. */
type TileType = ValidTiles | 'x'

/** All of the variants supported by the engine. */
type TraxVariant = 'trax' | 'traxloop' | 'trax8'

/** Tile type names are determined by listing the line color at each edge,
 * starting from the top and going clockwise, and then sorted alphabetically
 * and given a single letter name, so 'bbww' becomes 'a', which gives us six
 * different tile names: a-f, as follows:
 *     a         b        c        d        e        f
 *  +--#--+   +--#--+  +--#--+  +--o--+  +--o--+  +--o--+
 *  |   # |   |  #  |  | #   |  | o   |  |  o  |  |   o |
 *  oo   ##   ooo#ooo  ##   oo  oo   ##  #######  ##   oo
 *  | o   |   |  #  |  |   o |  |   # |  |  o  |  | #   |
 *  +--o--+   +--#--+  +--o--+  +--#--+  +--o--+  +--#--+
 */
type ValidTiles = 'a' | 'b' | 'c' | 'd' | 'e' | 'f'
