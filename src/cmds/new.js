/* Copyright 2022 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// CLI new command

export const newCmd = {
  name: 'new',
  alt: 'start',
  args: '[variant] [[p1 name] "vs" [p2 name]]',
  comp: 'trax|loop|8x8 vs',
  desc: 'start a new game',
  help: [
    'Valid variants are "trax", "loop", and "8x8". Defaults to "trax" if not',
    'specified. Use the "vs" keyword to use specific names for the players.',
  ],
}

// Okay, yes, this is silly
const pairs = [
  ['Abbott', 'Costello'],
  ['Adam', 'Eve'],
  ['Anne Boleyn', 'Henry VIII'],
  ['Ash', 'Pikachu'],
  ['Barbie', 'Ken'],
  ['Batman', 'Robin'],
  ['Bert', 'Ernie'],
  ['Bonnie', 'Clyde'],
  ['Calvin', 'Hobbes'],
  ['Chip', 'Dale'],
  ['Donald', 'Daisy'],
  ['Fred', 'Barney'],
  ['Fred', 'George'],
  ['Frodo', 'Samwise'],
  ['Han', 'Chewie'],
  ['Harry', 'Ron'],
  ['Holmes', 'Watson'],
  ['Jekyll', 'Hyde'],
  ['Jim', 'Pam'],
  ['Joe', 'Volcano'],
  ['John', 'Yoko'],
  ['King Kong', 'Godzilla'],
  ['Lewis', 'Clark'],
  ['Lilo', 'Stitch'],
  ['Luke', 'Leia'],
  ['Marge', 'Homer'],
  ['Mario', 'Luigi'],
  ['Marlin', 'Dory'],
  ['Mary-Kate', 'Ashley'],
  ['Mickey', 'Minnie'],
  ['Miss Piggy', 'Kermit'],
  ['Mork', 'Mindy'],
  ['Obi-Wan', 'Anakin'],
  ['Pan', 'Hook'],
  ['Phineas', 'Ferb'],
  ['player 1', 'player 2'],
  ['p1', 'p2'],
  ['R2-D2', 'C-3PO'],
  ['Ren', 'Stimpy'],
  ['Rick', 'Morty'],
  ['Romeo', 'Juliet'],
  ['Shaggy', 'Scooby'],
  ['Shrek', 'Fiona'],
  ['Simon', 'Garfunkel'],
  ['Snoopy', 'Woodstock'],
  ['Sonny', 'Cher'],
  ['Spongebob', 'Patrick'],
  ['Tarzan', 'Jane'],
  ['Thelma', 'Louise'],
  ['Thing 1', 'Thing 2'],
  ['Tom', 'Huckleberry'],
  ['Tom', 'Jerry'],
  ['Tweety', 'Sylvester'],
  ['Waldorf', 'Statler'],
  ['Wallace', 'Gromit'],
  ['white', 'black'],
  ['Woody', 'Buzz'],
]

const pairUp = () => {
  const duo = pairs[Math.floor(Math.random() * pairs.length)]
  if (duo[0].toLowerCase() !== duo[0] && Math.random() < 0.5) duo.reverse()
  return duo
}

newCmd.fn = (CLI, variant, ...names) => {
  let rules = ''
  if (variant) {
    variant = variant.toLowerCase()
    if (variant.length > 1 && 'puzzles'.startsWith(variant)) {
      return CLI.do('puzzles', 'new', ...names)
    }

    if (['8x8', '8', 'trax8x8', '8trax', '8x8trax'].includes(variant)) {
      rules = 'trax8'
    }

    if (['loop', 'l', 'looptrax'].includes(variant)) rules = 'traxloop'
    if (variant === 't') rules = 'trax'
    if (['trax', 'traxloop', 'trax8'].includes(variant)) rules = variant

    if (!rules) {
      if (names.includes('vs') || names.includes('VS')) {
        // We don't have a variant, we have player names
        names.unshift(variant)
      } else {
        CLI.error('Unknown Trax variant.')
        return CLI.do('help', 'new')
      }
    }
  }

  const players = pairUp()
  if (names.length > 0) {
    let vs = names.indexOf('vs')
    if (vs === -1) vs = names.indexOf('VS')
    if (vs >= 0) {
      players[0] = names.slice(0, vs).join(' ') || 'white'
      players[1] = names.slice(vs + 1).join(' ') || 'black'
    }
  }

  const id = CLI.newGame(rules, players, '')
  CLI.out(CLI.color('Started new game #' + id))
}
