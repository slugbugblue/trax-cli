/** Notes CLI command.
 * @copyright 2022
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

export const notesCmd = {
  name: 'notes',
  args: '[#id] <text>',
  desc: 'add a note to a game',
  help: [
    'Add a short note to a game that will be shown when the game is',
    'displayed, listed, or exported.',
  ],
}

notesCmd.fn = (CLI, id, ...note) => {
  const current = String(CLI.GAME.id)
  if (!id) id = current
  if (!/^#?\d+$/.test(id)) {
    note.unshift(id)
    id = current
  }

  if (id.startsWith('#')) id = id.slice(1)

  const game = CLI.GAMES[id]

  if (!game) return CLI.error('Game not found.')

  const notes = game.notes || []
  const move = game.moves.length > 0 ? game.moves.split(' ').length : 0

  notes.push({ move, note: note.join(' ') })
  game.notes = notes
  CLI.save()
}
