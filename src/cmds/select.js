/* Copyright 2022 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// CLI select command

export const selectCmd = {
  name: 'select',
  alt: ['choose', '#'],
  args: '#id [command]',
  comp: '<id> <cmd>',
  desc: 'select a different game',
  rx: /^#\d+$/,
  help: [
    'Make another game the default game of future commands. If another',
    'command is included, that command will be run immediately.',
  ],
}

selectCmd.fn = (CLI, id, ...cmds) => {
  if (!id) {
    CLI.error('Missing id.')
    return CLI.do('help', 'select')
  }

  if (id.startsWith('#')) id = id.slice(1)
  CLI.load(id)
  if (String(CLI.GAME?.id) === id) {
    CLI.out(CLI.color(`Game #${id} selected.`))
    if (cmds.length > 0) {
      CLI.doNext(cmds.join(' '))
    }
  } else {
    CLI.error('Invalid id. Type "list" to see available games.')
  }
}
