/* Copyright 2022 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// CLI list command

import process from 'node:process'

import { Trax } from '@slugbugblue/trax'

export const listCmd = {
  name: 'list',
  alt: 'ls',
  args: '[sort|filter...] ["reverse"]',
  desc: 'list all games',
  comp:
    'id|number game|rules|name player1|p1 player2|p2 move|status turn' +
    ' active|over trax|8x8|loop reverse',
  help: [
    'Valid sort options are:',
    '  "id": sort by game id',
    '  "game": sort by game type',
    '  "player1" or "player2": sort by player name',
    '  "moves": sort by the number of moves',
    'Available filters:',
    '  #id: match a game or a certain number of moves',
    '  "active" or "over": whether a game is in progress or not',
    '  "loop" or "8x8" or "trax": filter by game type',
    "  <other text>: match a player's name or the latest game note",
    'Add the "reverse" option to reverse the sort order.',
  ],
}

const alphaSort = (a, b) => {
  a = String(a).toLowerCase()
  b = String(b).toLowerCase()
  if (a < b) return -1
  if (b < a) return 1
  return 0
}

const sortBy = (sort) => {
  if (!sort) return 'id'
  sort = sort.toLowerCase()
  if (sort === '#') return 'sid'
  if ('ids'.startsWith(sort)) return 'sid'
  if ('games'.startsWith(sort)) return 'rules'
  if ('rules'.startsWith(sort)) return 'rules'
  if ('names'.startsWith(sort)) return 'rules'
  if ('numbers'.startsWith(sort)) return 'sid'
  if ('moves'.startsWith(sort)) return 'smoves'
  if ('status'.startsWith(sort)) return 'smoves'
  if ('turn'.startsWith(sort)) return 'sturn'
  if ('player'.startsWith(sort) && sort.endsWith('1')) return 'p1'
  if ('player'.startsWith(sort) && sort.endsWith('2')) return 'p2'
  if ('reverse'.startsWith(sort)) return 'reverse'
  return sort
}

const listSort = (list, filters) => {
  if (list.length === 0) return list
  let reverse = false
  for (const sort of filters) {
    const by = sortBy(sort)
    if (by === 'reverse') {
      reverse = true
    } else if (list[0]?.[by]) {
      list.sort((a, b) => alphaSort(a[by], b[by]))
    } else if (by === 'trax') {
      list = list.filter((g) => g.rules === by)
    } else if (['over', 'active'].includes(by)) {
      list = list.filter((g) => (by === 'over' ? g.over : !g.over))
    } else {
      list = list.filter((g) => {
        return (
          g.id.endsWith(by) ||
          Trax.names[g.rules].toLowerCase().includes(by) ||
          g.p1.toLowerCase().includes(by) ||
          g.p2.toLowerCase().includes(by) ||
          g.moves.startsWith(by + ' ') ||
          g.note.includes(by)
        )
      })
    }
  }

  if (reverse) list.reverse()

  return list
}

/** Find how many columns are left over after all the other columns arefilled.
 * @arg {Record<string, number>} sizes
 * @returns {number} number of columns left over
 */
const leftover = (sizes) => {
  let left = process.stdout.columns
  for (const size of Object.values(sizes)) {
    left -= size
  }

  return left
}

listCmd.fn = (CLI, ...filters) => {
  if (Object.keys(CLI.GAMES).length === 0) {
    return CLI.error('No games. Type "new" to start a new game.')
  }

  const puzzle = filters.find((f) => f.length > 1 && 'puzzles'.startsWith(f))
  if (puzzle) return CLI.do('puzzles', ...filters.filter((f) => f !== puzzle))

  let list = []
  const size = {
    spacing: 13, // Amount of space taken up by fixed-length and between columns
    id: 1,
    name: 1,
    moves: 1,
    p1: 1,
    p2: 1,
  }
  let noteSize = Number.POSITIVE_INFINITY

  for (const game of Object.values(CLI.GAMES)) {
    const moves = game.moves.length === 0 ? 0 : game.moves.split(' ').length
    list.push({
      sid: String(game.id).padStart(9, '0'),
      id: (game.id === CLI.GAME.id ? '* #' : '#') + String(game.id),
      name: game.name?.length || 0,
      rules: game.rules,
      moves: game.over ? 'game over' : CLI.plural(moves, 'move'),
      smoves: game.over ? '9999' : String(moves).padStart(9, '0'),
      sturn: String(game.turn),
      p1: game.players?.[0] || 'white',
      p2: game.players?.[1] || 'black',
      turn: game.turn,
      over: game.over,
      note: game.notes?.[game.notes.length - 1]?.note || '',
    })
  }

  list = listSort(list, filters)

  for (const game of list) {
    size.id = Math.max(size.id, game.id.length)
    size.name = Math.max(size.name, game.name)
    size.moves = Math.max(size.moves, game.moves.length)
    size.p1 = Math.max(size.p1, game.p1.length)
    size.p2 = Math.max(size.p2, game.p2.length)
    noteSize = Math.min(noteSize, leftover(size))
  }

  for (const game of list) {
    CLI.out(
      CLI.color(game.id.padStart(size.id) + ' ') +
        Trax.names[game.rules] +
        ' '.repeat(size.name - game.name + 1) +
        CLI.bubble(game.turn === 1 ? 'wh' : 'w', game.p1) +
        CLI.color('vs '.padStart(size.p1 - game.p1.length + 4)) +
        CLI.bubble(game.turn === 2 ? 'bh' : 'b', game.p2) +
        CLI.color(' '.padStart(size.p2 - game.p2.length + 1)) +
        CLI.color(game.moves.padEnd(size.moves)) +
        CLI.color.help(' ' + game.note.slice(0, noteSize)),
    )
  }
}
