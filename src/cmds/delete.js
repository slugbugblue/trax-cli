/* Copyright 2022 Chad Transtrum
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// CLI delete command

export const deleteCmd = {
  name: 'delete',
  alt: ['remove', 'rm'],
  args: '[#id] ["force"]',
  comp: '<id> force',
  desc: 'delete a game',
  help: [
    'Delete the current game. Or specify a game number to delete that game.',
    'If the game is in progress, you must type "force" to delete it.',
  ],
}

const selectAnyGame = (CLI) => {
  if (CLI.GAMES) {
    const games = Object.values(CLI.GAMES).sort((a, b) => {
      // Active games first
      if (a.over && !b.over) return 1
      if (b.over && !a.over) return -1
      // Games with more moves first
      return (b.moves?.length || 0) - (a.moves?.length || 0)
    })
    if (games.length > 0) {
      CLI.do('select', String(games[0].id))
    }
  }
}

deleteCmd.fn = (CLI, id, force = 'x') => {
  const current = String(CLI.GAME.id)
  if (!id) id = current
  if ('force'.startsWith(id)) {
    id = current
    force = 'force'
  }

  if (id.startsWith('#')) id = id.slice(1)

  const game = CLI.GAMES[id]

  if (!game || !String(game.id)) {
    return CLI.error('Invalid id. Type "list" to see available games.')
  }

  if (game.over || game.moves === '' || 'force'.startsWith(force)) {
    CLI.delete(id)
    CLI.out(CLI.color('Deleted game #' + id + '.'))

    if (id === String(current)) {
      selectAnyGame(CLI)
    }
  } else {
    CLI.error('Game #' + id + ' is still active. Use "force" to delete it.')
  }
}
